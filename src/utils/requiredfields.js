// eslint-disable-next-line no-unused-vars
import _ from 'lodash';

export const verifyRequiredFields = (fields, requiredFields) => {
  const test = requiredFields.map((item) => {
    if (!fields.includes(item)) {
      return false;
    }
    return true;
  });
  return (!test.includes(false));
};
