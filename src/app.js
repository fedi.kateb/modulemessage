import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import routes from './api/routes/msgRoutes';
import templateroutes from './api/routes/traductionRoutes';
import {config} from 'dotenv';







config({path: `${process.cwd()}/.env`})
const host = process.env.DB_HOST ;

const dbname = process.env.DB_NAME;


mongoose.connect(`mongodb://${host}/${dbname}`);

const app = express() ;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);
templateroutes(app);
export default app;

