import request from 'supertest';
import app from '../app';
import cnx from '../infrastructure/connectMongo';

describe('Test the root path', () => {
/* 
  beforeEach(() => {
    
    cnx.clearDB();
  });
  afterEach(() => {
    cnx.clearDB();
  });
 */
let variableid = {} ;
  
    // test post add template without ERROR
    test('It should response the POST add variable',
     async (done) => {

      const response = await request(app).post('/template').send({
        variable: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          label_Code: 'test',
        },
        label_Content: 'test', 

      });

      variableid['id']= response.body.Variable._id
      console.log('*******************************************************************',variableid.id);
      
      expect(response.status).toBe(200);
      expect(response.body.msg).toEqual('variable added');
    done();
    });
  
  
  
  
    // test post with error : required campany_Id
    test('It should response the POST add template with error required campany_Id',
      async (done) => {
        const response = await request(app).post('/template').send({
          variable: {
            application_Id: 'test',
            module_Id: 'test',
            lang_Id: 'test',
            label_Code: 'test',
          },
          label_Content: 'test', 
        });
        expect(response.status).toBe(400);
        expect(response.body.msg).toEqual('Missing required Fields')
       
        done();
      });
  
  // test post with error : required application_Id
  test('It should response the POST add template with error required application_Id',
  async (done) => {
    const response = await request(app).post('/template').send({
      variable: {
        campany_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
        label_Code: 'test',
      },
      label_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });


  // test post with error : required module_Id
  test('It should response the POST add template with error required module_Id',
  async (done) => {
    const response = await request(app).post('/template').send({
      variable: {
        campany_Id: 'test',
        application_Id: 'test',
        lang_Id: 'test',
        label_Code: 'test',
      },
      label_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });




  // test post with error : required lang_Id
  test('It should response the POST add template with error required lang_Id',
  async (done) => {
    const response = await request(app).post('/template').send({
      variable: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        label_Code: 'test',
      },
      label_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });



  // test post with error : required label_Code
  test('It should response the POST add template with error required label_Code',
  async (done) => {
    const response = await request(app).post('/template').send({
      variable: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
      },
      label_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });



  // test post with error : required label_Content
  test('It should response the POST add template with error required label_Content',
  async (done) => {
    const response = await request(app).post('/template').send({
      variable: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
        label_Code: 'test1',
      }
     });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });
  

    // test post add template with ERROR template already exist 
        test('It should response the POST add template with ERROR template already exist',
     async (done) => {
      const response = await request(app).post('/template').send({
        variable: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          label_Code: 'test',
        },
        label_Content: 'test', 

      });
      expect(response.status).toBe(400);
      expect(response.body.msg).toEqual('variable is already exist');
    done();
    });
  


  
   

         // test Get : get msgbyCode without ERROR
   test('It should response the GET template method without ERROR',
        async (done) => {
          const response = await request(app).get('/template')
          expect(response.status).toBe(200);
          done();
        }); 




     // test PUT update template without ERROR
    test('It should response the PUT update template', async (done) => {
      const response = await request(app).put('/template').send({
        variable: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          label_Code: 'test',
        },
        label_Content: 'testupdate', 
      });
      expect(response.status).toBe(200);
      expect(response.body.msg).toEqual('variable updated');
      done();
    }); 

 // test PUT update template with ERROR campany_Id required
 test('It should response the PUT update template with ERROR campany_Id required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      label_Code: 'test',
    },
    label_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 





 // test PUT update template with ERROR application_Id required
 test('It should response the PUT update template with ERROR application_Id required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      label_Code: 'test',
    },
    label_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update template with ERROR module_Id required
 test('It should response the PUT update template with ERROR module_Id required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'test',
      application_Id: 'test',
      lang_Id: 'test',
      label_Code: 'test',
    },
    label_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update template with ERROR lang_Id required
 test('It should response the PUT update template with ERROR lang_Id required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      label_Code: 'test',
    },
    label_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update template with ERROR label_Code required
 test('It should response the PUT update template with ERROR label_Code required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
    },
    label_Content: 'testupdate'
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update template with ERROR label_Content required
 test('It should response the PUT update template with ERROR label_Content required', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      label_Code: 'test',
    }
    
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 


 // test PUT update template with ERROR template not found
 test('It should response the PUT update template with ERROR template not found', async (done) => {
  const response = await request(app).put('/template').send({
    variable: {
      campany_Id: 'testnot found',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      label_Code: 'test',
    },
    label_Content: 'testupdate'
    
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('variable not found');
  done();
}); 



 // test delete  template with ERROR Missing parameter Fields
 test('It should response the delete  template with ERROR Missing parameter Fields', async (done) => {
  const response = await request(app).delete('/template');
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing parameter Fields');
  done();
}); 

// test delete  template with ERROR template 'not found'
test('It should response the delete template with ERROR template not found', async (done) => {
  const response = await request(app).delete('/template').query({id:'00000000000000000000000000000'})
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('this Variable not found to delete');
  done();
}); 

// test delete  template without ERROR
test('It should response the PUT update template with ERROR template not found', async (done) => {
  const response = await request(app).delete('/template').query({id:variableid.id})
  expect(response.status).toBe(200);
  done();
}); 


});
