import request from 'supertest';
import app from '../app';
import cnx from '../infrastructure/connectMongo';

describe('Test the root path', () => {

let messageid = {} ;
  
    // test post add message without ERROR
    test('It should response the POST add message',
     async (done) => {

      const response = await request(app).post('/message').send({
        Msg_Code: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          message_Code: 'test',
        },
        msg_Content: 'test', 

      });

      messageid['id']= response.body.Message._id
      console.log('*******************************************************************',messageid.id);
      
      expect(response.status).toBe(200);
      expect(response.body.msg).toEqual('message added');
    done();
    });
  
  
  
  
    // test post with error : required campany_Id
    test('It should response the POST add message with error required campany_Id',
      async (done) => {
        const response = await request(app).post('/message').send({
          Msg_Code: {
            application_Id: 'test',
            module_Id: 'test',
            lang_Id: 'test',
            message_Code: 'test',
          },
          msg_Content: 'test', 
        });
        expect(response.status).toBe(400);
        expect(response.body.msg).toEqual('Missing required Fields')
       
        done();
      });
  
  // test post with error : required application_Id
  test('It should response the POST add message with error required application_Id',
  async (done) => {
    const response = await request(app).post('/message').send({
      Msg_Code: {
        campany_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
        message_Code: 'test',
      },
      msg_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });


  // test post with error : required module_Id
  test('It should response the POST add message with error required module_Id',
  async (done) => {
    const response = await request(app).post('/message').send({
      Msg_Code: {
        campany_Id: 'test',
        application_Id: 'test',
        lang_Id: 'test',
        message_Code: 'test',
      },
      msg_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });




  // test post with error : required lang_Id
  test('It should response the POST add message with error required lang_Id',
  async (done) => {
    const response = await request(app).post('/message').send({
      Msg_Code: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        message_Code: 'test',
      },
      msg_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });



  // test post with error : required message_Code
  test('It should response the POST add message with error required message_Code',
  async (done) => {
    const response = await request(app).post('/message').send({
      Msg_Code: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
      },
      msg_Content: 'test', 

    });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });



  // test post with error : required msg_Content
  test('It should response the POST add message with error required msg_Content',
  async (done) => {
    const response = await request(app).post('/message').send({
      Msg_Code: {
        campany_Id: 'test',
        application_Id: 'test',
        module_Id: 'test',
        lang_Id: 'test',
        message_Code: 'test1',
      }
     });
    expect(response.status).toBe(400);
    expect(response.body.msg).toEqual('Missing required Fields')
   
    done();
  });
  

    // test post add message with ERROR message already exist 
        test('It should response the POST add message',
     async (done) => {
      const response = await request(app).post('/message').send({
        Msg_Code: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          message_Code: 'test',
        },
        msg_Content: 'test', 

      });
      expect(response.status).toBe(400);
      expect(response.body.msg).toEqual('message is already exist');
    done();
    });
  


  
   

         // test Get : get msgbyCode without ERROR
   test('It should response the GET Message method without ERROR',
        async (done) => {
          const response = await request(app).get('/message')
          expect(response.status).toBe(200);
          done();
        }); 




     // test PUT update message without ERROR
    test('It should response the PUT update message', async (done) => {
      const response = await request(app).put('/message').send({
        Msg_Code: {
          campany_Id: 'test',
          application_Id: 'test',
          module_Id: 'test',
          lang_Id: 'test',
          message_Code: 'test',
        },
        msg_Content: 'testupdate', 
      });
      expect(response.status).toBe(200);
      expect(response.body.msg).toEqual('message updated');
      done();
    }); 

 // test PUT update message with ERROR campany_Id required
 test('It should response the PUT update message with ERROR campany_Id required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      message_Code: 'test',
    },
    msg_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 





 // test PUT update message with ERROR application_Id required
 test('It should response the PUT update message with ERROR application_Id required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      message_Code: 'test',
    },
    msg_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update message with ERROR module_Id required
 test('It should response the PUT update message with ERROR module_Id required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'test',
      application_Id: 'test',
      lang_Id: 'test',
      message_Code: 'test',
    },
    msg_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update message with ERROR lang_Id required
 test('It should response the PUT update message with ERROR lang_Id required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      message_Code: 'test',
    },
    msg_Content: 'testupdate', 
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update message with ERROR message_Code required
 test('It should response the PUT update message with ERROR message_Code required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
    },
    msg_Content: 'testupdate'
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 

 // test PUT update message with ERROR msg_Content required
 test('It should response the PUT update message with ERROR msg_Content required', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'test',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      message_Code: 'test',
    }
    
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing required Fields');
  done();
}); 


 // test PUT update message with ERROR message not found
 test('It should response the PUT update message with ERROR message not found', async (done) => {
  const response = await request(app).put('/message').send({
    Msg_Code: {
      campany_Id: 'testnot found',
      application_Id: 'test',
      module_Id: 'test',
      lang_Id: 'test',
      message_Code: 'test',
    },
    msg_Content: 'testupdate'
    
  });
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('message not found');
  done();
}); 



 // test delete  message with ERROR Missing parameter Fields
 test('It should response the delete  message with ERROR Missing parameter Fields', async (done) => {
  const response = await request(app).delete('/message');
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('Missing parameter Fields');
  done();
}); 

// test delete  message with ERROR message 'not found'
test('It should response the delete message with ERROR message not found', async (done) => {
  const response = await request(app).delete('/message').query({id:'00000000000000000000000000000'})
  expect(response.status).toBe(400);
  expect(response.body.msg).toEqual('this message not found to delete');
  done();
}); 

// test delete  message without ERROR
test('It should response the PUT update message with ERROR message not found', async (done) => {
  const response = await request(app).delete('/message').query({id:messageid.id})
  expect(response.status).toBe(200);
  done();
}); 


});
 