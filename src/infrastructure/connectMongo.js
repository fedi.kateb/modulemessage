import {config} from 'dotenv';
import mongoose from 'mongoose';

config({path: `${process.cwd()}/.env`});

const host = process.env.DB_HOST;
const dbname = process.env.DB_NAME;
const dbport = process.env.DB_PORT;

const initDB = () => {

  mongoose.connection
      .on('error', (error) => {
        console.log('MongoDB cnx Error', error);
      })
      .on('close', () => {
        console.log('MongoDB Closed');
      })
      .once('open', async () => {
        console.log('MongoDB Connected');
      });
  mongoose.connect(`mongodb://${host}:${dbport}/${dbname}`, {useNewUrlParser: true,useCreateIndex: true , useFindAndModify: false });
};
const clearDB = () => {
  mongoose.connect(`mongodb://${host}:${dbport}/${dbname}`, {useNewUrlParser: true}, function() {
    /* Drop the DB */
    mongoose.connection.db.dropDatabase();
  });
};
const disconnect=() => {
  mongoose.disconnect();
};
export default {
  initDB,
  disconnect,
  clearDB,
};
