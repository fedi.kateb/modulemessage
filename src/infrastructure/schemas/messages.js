const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const messageSchema = new Schema({
          Msg_Code: { 
                  campany_Id:  {
                  type: String,
                  required: true,
                  },

                  application_Id:  {
                  type: String,
                  required: true,
                  },

                    module_Id:  {
                      type: String,
                      required: true,
                  },

                  lang_Id:  {
                    type: String,
                    required: true,
                  },

                  message_Code:  {
                    type: String,
                    required: true,
                  },


  type:{},
  required: true,
  unique:true },
  

          msg_Content: {
            type: String,
            required: true,
          },

          status: {
            type: String,
            required: true,
            enum : ['A', 'D'],
            default: 'A'
          },


});

export default messageSchema;

