import { deleteVariableById } from '../../domains/templateTranslateService'
import { findVariableById } from '../../domains/templateTranslateService'



export const handlerDeleteVariable = async (req, res) => {

    const id = req.query.id;
    if (id == undefined) { res.status(400).send({ msg: 'Missing parameter Fields' }) }
    else {

        const exist = await findVariableById(id);
        if (exist.Variable) {
            const reslt = await deleteVariableById(id)
            if (reslt.msg) { res.status(200).send(reslt.msg) }
            else if (reslt.error) { res.status(400).send(reslt.error) }
        } else {
            res.status(400).send({ msg: 'this Variable not found to delete' })
        }

    }


}
