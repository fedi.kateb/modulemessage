import { createVariable } from '../../domains/templateTranslateService'
import { findVariable } from '../../domains/templateTranslateService'
import { Validator } from 'node-input-validator'
export const handlerAddVariable = async (req, res) => {

    const body = {
        campany_Id: req.body.variable.campany_Id,
        application_Id: req.body.variable.application_Id,
        module_Id: req.body.variable.module_Id,
        lang_Id: req.body.variable.lang_Id,
        label_Code: req.body.variable.label_Code,
        label_Content: req.body.label_Content,
    }
    const validator = new Validator(body, {
       campany_Id:    'required',
        application_Id:    'required',
        module_Id:     'required',
        lang_Id:    'required',
        label_Code:     'required',
        label_Content:     'required',

    });





   const existvariable = await findVariable(req.body);
   if(existvariable.Variable) {
    res.status(400).send({ msg: 'variable is already exist' }); 
}
else{


    validator.check().then(async (matched) => {
        if (matched) {
            const reslt = await createVariable(req.body);
            if (reslt.err) {
                res.status(400).send(reslt.err);


            } else if (reslt.Variable) {
                res.status(200).send({ msg: 'variable added', Variable: reslt.Variable });
            }
        }



        if (!matched) {
            res.status(400).send({ msg: 'Missing required Fields' })
        }



    });



};
}
