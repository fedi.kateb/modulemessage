
import { updateMsg } from '../../domains/msgService'
import { findMessage } from '../../domains/msgService'
import { Validator } from 'node-input-validator'
export const handlerUpdateMsg = async (req, res) => {
     // check if the body empty render an error 
if( await  req.body.Msg_Code == undefined) { res.status(400).send({ msg: 'body not found' });  }
    // create an object to get all variables sended by the user of the API 

const body = {
        campany_Id: req.body.Msg_Code.campany_Id,
        application_Id: req.body.Msg_Code.application_Id,
        module_Id: req.body.Msg_Code.module_Id,
        lang_Id: req.body.Msg_Code.lang_Id,
        message_Code: req.body.Msg_Code.message_Code,
        msg_Content: req.body.msg_Content,
    
    }
    //create an object validator to verify the existance of inputs of the API

    const validator = new Validator(body, {
        campany_Id: 'required',
        application_Id: 'required',
        module_Id: 'required',
        lang_Id: 'required',
        message_Code:'required',
        msg_Content: 'required',
    });



// excuse the validator to check if filds are empty 
    validator.check().then(async (matched) => {
        if (matched) {


                // check if this message exist or not to update it 
    const existmessage = await findMessage(req.body);
    if(!existmessage.Message) {
     res.status(400).send({ msg: 'message not found' }); 
 }
 else{
           // call update function            
            const reslt = await updateMsg(req.body);
            if (reslt.err) {
                res.status(400).send(reslt.err);
            } else if (reslt.Message) {
                res.status(200).send({ msg:'message updated', Message: reslt.Message });
            }}
        }
        if (!matched) {
            res.status(400).send({ msg: 'Missing required Fields' })
        }
    });
};

