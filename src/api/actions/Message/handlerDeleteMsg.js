


import { deleteMsgById } from '../../domains/msgService'
import { findMessageById } from '../../domains/msgService'



export const handlerDeleteMsg = async (req, res) => {
// get the id of the message to delete it 
    const id = req.query.id;
    // if the id is empty render a msg with msg 'messing fields'
    if (id == undefined) { res.status(400).send({ msg: 'Missing parameter Fields' }) }
    else {
        // check if the message to delete exist or not 
        const exist = await findMessageById(id);
        if (exist.Message) {
            // if this message exist call the function to delete it 
            const deleted = await deleteMsgById(id)
            console.log(deleted.msg)
            if (deleted.msg) {
                // if the message deleted show a result
                res.status(200).send({ msg: deleted.msg })
                console.log('ok ok ok ');}
                else if (deleted.err) {
                    // if the message can't be deleted show the error
                    res.status(400).send({ error: deleted.err })
                }
        } else {
            // if the messsage didn't exist render a msg 'not found'
            res.status(400).send({ msg: 'this message not found to delete' })
        }

    }


}
