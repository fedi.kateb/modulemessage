import { config } from 'dotenv';
import { findbyobject } from '../../domains/msgService'

config({ path: `${process.cwd()}/.env` });
export const handlerGet = async (req, res) => {     
    console.log('********************************************************frrr', req.query);
    
    // function to get the message depends of the paramaters sent in the request
    // it depend of the input variables in the query
    // input can be lang_id , message_Code , campany_Id , application_Id , module_Id 
    // the result is an array of messages
    const result = await findbyobject(req.query)
    if (result.error) {
        res.status(400).send({ msg: result.error });
    } else if (result.Message) {
        res.status(200).send({ messages: result.Message , msg : '' });
    }
}
