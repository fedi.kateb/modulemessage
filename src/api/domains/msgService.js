import mongoose from 'mongoose';
import MessageSchema from '../../infrastructure/schemas/messages';
const message = mongoose.model('MessageSchema', MessageSchema);

// find a message or a list of message using this function 
// input object 
// result : the function return messages
export const  findbyobject = async (object) => {
  console.log(object);

// parse the input object to search object that it will be useful in mongoose conditon 
const search = {}
  for (const key in object) {
  console.log( String('Msg_Code.'+key)  , String(object[key]) );
  search[String('Msg_Code.'+key)] = String(object[key])}
  search['status'] = 'A';

  try {
    const Message = await message.find(search);  
    return { Message };
  } catch (err) {
    return { err};
  }
};


export const createMsg = async (msgInfo) => {
  try {
    const newMsg = new message({
      Msg_Code: {
        campany_Id: msgInfo.Msg_Code.campany_Id,
        application_Id: msgInfo.Msg_Code.application_Id,
        module_Id: msgInfo.Msg_Code.module_Id,
        lang_Id: msgInfo.Msg_Code.lang_Id,
        message_Code: msgInfo.Msg_Code.message_Code,
      },
      msg_Content: msgInfo.msg_Content, 
    });
    const Message = await newMsg.save();
    return { Message };
  } catch (err) {
    return { err };
  }
};



export const findMessage = async (msgInfo) => {
  try {
 
    const Message = await message.findOne( 
      { 'Msg_Code.campany_Id': msgInfo.Msg_Code.campany_Id ,
      'Msg_Code.application_Id': msgInfo.Msg_Code.application_Id,
      'Msg_Code.module_Id': msgInfo.Msg_Code.module_Id,
      'Msg_Code.lang_Id':msgInfo.Msg_Code.lang_Id,
      'Msg_Code.message_Code': msgInfo.Msg_Code.message_Code
      });
  
    return {Message} ;
  } catch (err) {      
    return { err };
  }
};

export const deleteMsgById = async (id) => {
  try {
    const Message = await message.updateOne({'_id':id}, {'status' : 'D'}, {upsert:true});
    return { msg : 'message is deleted' };
  } catch (err) {
    return { err };
  }
};


export const findMessageById = async (id) => {
  try {
    const Message = await message.findById(id);
    console.log('Message', Message);
    return { Message };
  } catch (error) {
    return { error };
  }
};



export const updateMsg = async (msgInfo) => {
  console.log('eeeeeeeeeeeeeeeeeeee',msgInfo)
  try {
    const msg = await message.findOneAndUpdate(
      {    'Msg_Code.campany_Id': msgInfo.Msg_Code.campany_Id ,
      'Msg_Code.application_Id': msgInfo.Msg_Code.application_Id,
      'Msg_Code.module_Id': msgInfo.Msg_Code.module_Id,
      'Msg_Code.lang_Id':msgInfo.Msg_Code.lang_Id,
      'Msg_Code.message_Code': msgInfo.Msg_Code.message_Code },
      { 'msg_Content': msgInfo.msg_Content },
      {upsert:true}
      );
      const Message = await findMessage(msgInfo);
    return { Message };
  } catch (err) {
    return { err };
  }
}