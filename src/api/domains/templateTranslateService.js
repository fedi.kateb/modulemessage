import mongoose from 'mongoose';
import templateTranslate from '../../infrastructure/schemas/templateTranslate';
const variable = mongoose.model('templateTranslateSchema',templateTranslate);

export const  findbyobject = async (object) => {
  console.log(object);

const search = {}
  for (const key in object) {
    console.log(
                   String('variable.'+key)  , String(object[key]) );
      search[String('variable.'+key)] = String(object[key])       
                   }

                   search['status'] = 'A';

  try {
    const Variable = await variable.find(search);
   
       
    return { Variable };
  } catch (err) {
    return { err};
  }
};

  export const createVariable = async (variableInfo) => {
    try {
      const newVariable = new variable({
        variable: {
          campany_Id: variableInfo.variable.campany_Id,
          application_Id: variableInfo.variable.application_Id,
          module_Id: variableInfo.variable.module_Id,
          lang_Id: variableInfo.variable.lang_Id,
          label_Code: variableInfo.variable.label_Code
        },
        label_Content: variableInfo.label_Content,
      });
      const Variable = await newVariable.save();
      return { Variable };
    } catch (err) {
      return { err };
    }
  };
 
  export const findVariable = async (variableInfo) => {
    console.log(variableInfo);
    
    try {
      const Variable = await variable.findOne( 
        { 
       'variable.campany_Id': variableInfo.variable.campany_Id ,
        'variable.application_Id': variableInfo.variable.application_Id,
        'variable.module_Id': variableInfo.variable.module_Id,
        'variable.lang_Id': variableInfo.variable.lang_Id,
        'variable.label_Code': variableInfo.variable.label_Code
        });
      return {Variable} ;
    } catch (error) {      
      return { error };
    }
  };





  export const updateVariable = async (variableInfo) => {
    console.log('eeeeeeeeeeeeeeeeeeee',variableInfo)
    try {
      const updvariable = await variable.findOneAndUpdate(
        {    'variable.campany_Id': variableInfo.variable.campany_Id ,
        'variable.application_Id': variableInfo.variable.application_Id,
        'variable.module_Id': variableInfo.variable.module_Id,
        'variable.lang_Id': variableInfo.variable.lang_Id,
        'variable.label_Code': variableInfo.variable.label_Code },
        { 'label_Content': variableInfo.label_Content },
        {upsert:true}
        ); 
        const Variable = await findVariable(variableInfo);
      return { Variable };
    } catch (err) {
      return { err };
    }
  
  }
  
export const deleteVariableById = async (id) => {
  console.log('i am here ')
  try {

    const Variable = await variable.updateOne({'_id':id}, {'status' : 'D'}, {upsert:true});
    return { msg : 'variable is deleted' };
  } catch (error) {
    return { error };
  }
};

export const findVariableById = async (id) => {
  try {

    const Variable = await variable.findById(id);
    console.log(Variable)
    return { Variable };
  } catch (error) {
    return { error };
  }
};
