import {handlerDeleteMsg}  from '../actions/Message/handlerDeleteMsg';
import {handlerAddMsg}  from '../actions/Message/handlerAddMsg';
import {handlerUpdateMsg} from '../actions/Message/handlerUpdateMsg';
import {handlerGet} from '../actions/Message/handlerGet';

 const routes = app => {

      app.route('/message')
// route /message with post method to add a message
      .post(handlerAddMsg)
// route /message with delete method to delete a message
      .delete(handlerDeleteMsg)
// route /message with put method to update a message 
      .put(handlerUpdateMsg)
// route /message with get method to get a message or an array of message 
      .get(handlerGet)

  }
 export default routes;
 