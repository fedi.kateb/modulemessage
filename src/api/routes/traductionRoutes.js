import {handlerGet} from '../actions/template/handlerGet'
import {handlerAddVariable} from '../actions/template/handlerAddVariable'
import {handlerUpdateVariable} from '../actions/template/handlerUpdateVariable'
import {handlerDeleteVariable}  from '../actions/template/handlerDeleteVariable';

const templateroutes = app => {
      app.route('/template')
// route /template with post method to add a variable   
      .post(handlerAddVariable)
// route /template with put method to update a variable   
      .put(handlerUpdateVariable)
// route /template with delete method to delete a variable   
      .delete(handlerDeleteVariable)
// route /template with get method to get a variable or an array of variable 
      .get(handlerGet)

  }
 export default templateroutes;
 