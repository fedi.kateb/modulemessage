import app from './app';
import {config} from 'dotenv';


config({path: `${process.cwd()}/.env`})
const port = process.env.APP_PORT || 8082 ;
try {
    app.listen(port,
        ()=> {
            console.log(`server is running at port  ${port}`);
            
        })
} catch (err){
    console.log(err);
    
}